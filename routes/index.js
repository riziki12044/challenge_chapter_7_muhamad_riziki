const route = require("express").Router()
const {UserController} = require("../controller")


route.post("/register", UserController.register)


module.exports = route