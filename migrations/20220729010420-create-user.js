'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      name: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Users');
  }
};

// 'use strict';
// module.exports = {
//   up. async (QueryInterface, sequelize) => {
//     await QueryInterface.createTable('user', {
//       id: {
//         allowNulls: false,
//         autoInrement: true,
//         type: sequelize.INTEGER
//       },
//       name: {
//         type: sequelize.STRING
//       },
//       password: {
//         type: sequelize.STRING
//       },
//       createdAt: {
//         allowNull: false,
//         type: sequelize.Date
//       },
//       updatedAt: {
//         allowNull: false,
//         type: sequelize.Date
//       }
//     });
//   },
//   down: async (QueryInterface, sequelize) => {
//     await QueryInterface.dropTable('Users');
//   }
// };